import {CookieService} from 'angular2-cookie/core';
import {Injectable} from "@angular/core";

// export const MENU = [
//   ...ADMIN_MENU
// ];
const MENU_ADMIN : any[] = [
  {
    path: '',
    children: [
      {
        path: 'administrator',
        data: {
          menu: {
            title: 'Administrator',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        },
        children: [
          {
            path: '',
            data: {
              menu: {
                title: 'User Profile',
              }
            },
            children: [
              {
                path: 'create',
                data: {
                  menu: {
                    title: 'Create',
                  }
                }
              },
              {
                path: 'update',
                data: {
                  menu: {
                    title: 'Update',
                  }
                }
              },
              {
                path: 'delete',
                data: {
                  menu: {
                    title: 'Delete',
                  }
                }
              },
              {
                path: 'approve',
                data: {
                  menu: {
                    title: 'Approve',
                  }
                }
              }
            ]
          },
          {
            path: 'log',
            data: {
              menu: {
                title: 'Log',
              }
            },
            children: [
              {
                path: 'monitoring',
                data: {
                  menu: {
                    title: 'Monitoring',
                  }
                }
              },
              {
                path: 'backup',
                data: {
                  menu: {
                    title: 'Backup',
                  }
                }
              },
              {
                path: 'clear',
                data: {
                  menu: {
                    title: 'Clear',
                  }
                }
              }
            ]
          }
        ]
      },
      {
        path: 'setting',
        data: {
          menu: {
            title: 'Setting',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        },
        children: [
          {
            path: 'wilayah',
            data: {
              menu: {
                title: 'Wilayah',
              }
            },
            children: [
              {
                path: 'regional',
                data: {
                  menu: {
                    title: 'Regional',
                  }
                }
              },
              {
                path: 'provinsi',
                data: {
                  menu: {
                    title: 'Provinsi',
                  }
                }
              },
              {
                path: 'kabupaten',
                data: {
                  menu: {
                    title: 'Kabupaten',
                  }
                }
              },
              {
                path: 'kecamatan',
                data: {
                  menu: {
                    title: 'Kecamatan',
                  }
                }
              },
              {
                path: 'kelurahan',
                data: {
                  menu: {
                    title: 'Kelurahan / Desa',
                  }
                }
              },
              {
                path: 'kodepos',
                data: {
                  menu: {
                    title: 'Kode Pos',
                  }
                }
              }
            ]
          },
          {
            path: 'dinas',
            data: {
              menu: {
                title: 'Wilayah Dinas',
              }
            },
            children: [
              {
                path: 'provinsi',
                data: {
                  menu: {
                    title: 'Provinsi',
                  }
                }
              },
              {
                path: 'kabupaten',
                data: {
                  menu: {
                    title: 'Kabupaten',
                  }
                }
              }
            ]
          }
        ]
      },
      {
        path: 'parameter',
        data: {
          menu: {
            title: 'Parameter',
            icon: 'ion-edit',
            selected: false,
            expanded: false,
            order: 100,
          }
        },
        children: [
          {
            path: 'jenis',
            data: {
              menu: {
                title: 'Jenis Koperasi',
              }
            }
          }
        ]
      },
      {
        path: '',
        data: {
          menu: {
            title: 'Taksonomi',
            icon: 'ion-stats-bars',
            selected: false,
            expanded: false,
            order: 200,
          }
        },
        children: [
          {
            path: 'taksonomi',
            data: {
              menu: {
                title: 'COA',
              }
            },
            children: [
              {
                path: 'jenis',
                data: {
                  menu: {
                    title: 'Jenis Rekening',
                  }
                }
              },
              {
                path: 'kelompok',
                data: {
                  menu: {
                    title: 'Kelompok Rekening',
                  }
                }
              },
              {
                path: 'bukubesar',
                data: {
                  menu: {
                    title: 'Buku Besar',
                  }
                }
              },
              {
                path: 'subbukubesar',
                data: {
                  menu: {
                    title: 'Sub Buku Besar',
                  }
                }
              }
            ]
          },
          {
            path: 'from',
            data: {
              menu: {
                title: 'FORM Laporan',
              }
            },
            children: [
              {
                path: '1',
                data: {
                  menu: {
                    title: 'FORM 01',
                  }
                }
              },
              {
                path: '2',
                data: {
                  menu: {
                    title: 'FORM 02',
                  }
                }
              },
              {
                path: '3',
                data: {
                  menu: {
                    title: 'FORM 03',
                  }
                }
              },
              {
                path: '4',
                data: {
                  menu: {
                    title: 'FORM 04',
                  }
                }
              },
              {
                path: '5',
                data: {
                  menu: {
                    title: 'FORM 05',
                  }
                }
              },
              {
                path: '6',
                data: {
                  menu: {
                    title: 'FORM 06',
                  }
                }
              }
            ]
          }
        ]
      },
      {
        path: 'laporan',
        data: {
          menu: {
            title: 'Laporan',
            icon: 'ion-android-laptop',
            selected: false,
            expanded: false,
            order: 300,
          }
        },
        children: [
          {
            path: 'instance',
            data: {
              menu: {
                title: 'Create Instance',
              }
            }
          },
          {
            path: 'verify',
            data: {
              menu: {
                title: 'Verifikasi',
              }
            }
          },
          {
            path: 'export',
            data: {
              menu: {
                title: 'Export',
              }
            }
          }
        ]
      },
      {
        path: 'monitoring',
        data: {
          menu: {
            title: 'Monitoring & Evaluasi',
            icon: 'ion-compose',
            selected: false,
            expanded: false,
            order: 400,
          }
        },
        children: [
          {
            path: 'tables',
            data: {
              menu: {
                title: 'Mode Table',
              }
            }
          },
          {
            path: 'maps',
            data: {
              menu: {
                title: 'Mode Peta',
              }
            }
          }
        ]
      }
    ]
  }
];
const MENU_ANONYM : any[] = [
  {
    path: '',
    children: [
      {
        path: 'administrator',
        data: {
          menu: {
            title: 'Administrator',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        },
        children: [
          {
            path: '',
            data: {
              menu: {
                title: 'User Profile',
              }
            },
            children: [
              {
                path: 'create',
                data: {
                  menu: {
                    title: 'Create',
                  }
                }
              },
              {
                path: 'update',
                data: {
                  menu: {
                    title: 'Update',
                  }
                }
              },
              {
                path: 'delete',
                data: {
                  menu: {
                    title: 'Delete',
                  }
                }
              },
              {
                path: 'approve',
                data: {
                  menu: {
                    title: 'Approve',
                  }
                }
              }
            ]
          },
          {
            path: 'log',
            data: {
              menu: {
                title: 'Log',
              }
            },
            children: [
              {
                path: 'monitoring',
                data: {
                  menu: {
                    title: 'Monitoring',
                  }
                }
              },
              {
                path: 'backup',
                data: {
                  menu: {
                    title: 'Backup',
                  }
                }
              },
              {
                path: 'clear',
                data: {
                  menu: {
                    title: 'Clear',
                  }
                }
              }
            ]
          }
        ]
      },
      {
        path: 'monitoring',
        data: {
          menu: {
            title: 'Monitoring & Evaluasi',
            icon: 'ion-compose',
            selected: false,
            expanded: false,
            order: 400,
          }
        },
        children: [
          {
            path: 'tables',
            data: {
              menu: {
                title: 'Mode Table',
              }
            }
          },
          {
            path: 'maps',
            data: {
              menu: {
                title: 'Mode Peta',
              }
            }
          }
        ]
      }
    ]
  }
];
const MENU_KOPERASI : any[] = [
  {
    path: '',
    children: [
      {
        path: 'administrator',
        data: {
          menu: {
            title: 'Administrator',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        },
        children: [
          {
            path: '',
            data: {
              menu: {
                title: 'User Profile',
              }
            },
            children: [
              {
                path: 'create',
                data: {
                  menu: {
                    title: 'Create',
                  }
                }
              },
              {
                path: 'update',
                data: {
                  menu: {
                    title: 'Update',
                  }
                }
              },
              {
                path: 'delete',
                data: {
                  menu: {
                    title: 'Delete',
                  }
                }
              },
              {
                path: 'approve',
                data: {
                  menu: {
                    title: 'Approve',
                  }
                }
              }
            ]
          },
          {
            path: 'log',
            data: {
              menu: {
                title: 'Log',
              }
            },
            children: [
              {
                path: 'monitoring',
                data: {
                  menu: {
                    title: 'Monitoring',
                  }
                }
              },
              {
                path: 'backup',
                data: {
                  menu: {
                    title: 'Backup',
                  }
                }
              },
              {
                path: 'clear',
                data: {
                  menu: {
                    title: 'Clear',
                  }
                }
              }
            ]
          }
        ]
      },
      {
        path: 'setting',
        data: {
          menu: {
            title: 'Setting',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        },
        children: [
          {
            path: 'wilayah',
            data: {
              menu: {
                title: 'Wilayah',
              }
            },
            children: [
              {
                path: 'regional',
                data: {
                  menu: {
                    title: 'Regional',
                  }
                }
              },
              {
                path: 'provinsi',
                data: {
                  menu: {
                    title: 'Provinsi',
                  }
                }
              },
              {
                path: 'kabupaten',
                data: {
                  menu: {
                    title: 'Kabupaten',
                  }
                }
              },
              {
                path: 'kecamatan',
                data: {
                  menu: {
                    title: 'Kecamatan',
                  }
                }
              },
              {
                path: 'kelurahan',
                data: {
                  menu: {
                    title: 'Kelurahan / Desa',
                  }
                }
              },
              {
                path: 'kodepos',
                data: {
                  menu: {
                    title: 'Kode Pos',
                  }
                }
              }
            ]
          },
          {
            path: 'dinas',
            data: {
              menu: {
                title: 'Wilayah Dinas',
              }
            },
            children: [
              {
                path: 'provinsi',
                data: {
                  menu: {
                    title: 'Provinsi',
                  }
                }
              },
              {
                path: 'kabupaten',
                data: {
                  menu: {
                    title: 'Kabupaten',
                  }
                }
              }
            ]
          }
        ]
      },
      {
        path: 'parameter',
        data: {
          menu: {
            title: 'Parameter',
            icon: 'ion-edit',
            selected: false,
            expanded: false,
            order: 100,
          }
        },
        children: [
          {
            path: 'jenis',
            data: {
              menu: {
                title: 'Jenis Koperasi',
              }
            }
          }
        ]
      },
      {
        path: '',
        data: {
          menu: {
            title: 'Taksonomi',
            icon: 'ion-stats-bars',
            selected: false,
            expanded: false,
            order: 200,
          }
        },
        children: [
          {
            path: 'taksonomi',
            data: {
              menu: {
                title: 'COA',
              }
            },
            children: [
              {
                path: 'jenis',
                data: {
                  menu: {
                    title: 'Jenis Rekening',
                  }
                }
              },
              {
                path: 'kelompok',
                data: {
                  menu: {
                    title: 'Kelompok Rekening',
                  }
                }
              },
              {
                path: 'bukubesar',
                data: {
                  menu: {
                    title: 'Buku Besar',
                  }
                }
              },
              {
                path: 'subbukubesar',
                data: {
                  menu: {
                    title: 'Sub Buku Besar',
                  }
                }
              }
            ]
          },
          {
            path: 'from',
            data: {
              menu: {
                title: 'FORM Laporan',
              }
            },
            children: [
              {
                path: '1',
                data: {
                  menu: {
                    title: 'FORM 01',
                  }
                }
              },
              {
                path: '2',
                data: {
                  menu: {
                    title: 'FORM 02',
                  }
                }
              },
              {
                path: '3',
                data: {
                  menu: {
                    title: 'FORM 03',
                  }
                }
              },
              {
                path: '4',
                data: {
                  menu: {
                    title: 'FORM 04',
                  }
                }
              },
              {
                path: '5',
                data: {
                  menu: {
                    title: 'FORM 05',
                  }
                }
              },
              {
                path: '6',
                data: {
                  menu: {
                    title: 'FORM 06',
                  }
                }
              }
            ]
          }
        ]
      },
      {
        path: 'laporan',
        data: {
          menu: {
            title: 'Laporan',
            icon: 'ion-android-laptop',
            selected: false,
            expanded: false,
            order: 300,
          }
        },
        children: [
          {
            path: 'instance',
            data: {
              menu: {
                title: 'Create Instance',
              }
            }
          },
          {
            path: 'verify',
            data: {
              menu: {
                title: 'Verifikasi',
              }
            }
          },
          {
            path: 'export',
            data: {
              menu: {
                title: 'Export',
              }
            }
          }
        ]
      },
      {
        path: 'monitoring',
        data: {
          menu: {
            title: 'Monitoring & Evaluasi',
            icon: 'ion-compose',
            selected: false,
            expanded: false,
            order: 400,
          }
        },
        children: [
          {
            path: 'tables',
            data: {
              menu: {
                title: 'Mode Table',
              }
            }
          },
          {
            path: 'maps',
            data: {
              menu: {
                title: 'Mode Peta',
              }
            }
          }
        ]
      }
    ]
  }
];
@Injectable()
export class AppMenu {



  constructor(private _cookieService : CookieService) {

  }


  getMenu(){
    var user = this._cookieService.get("user");
    if(user){
      let role = JSON.parse(user).role
      switch (role){
        case "ROLE_ADMINISTRATOR":
          return MENU_ADMIN;
        case "ROLE_KOPERASI":
          return MENU_KOPERASI;
      }
    }else{
      return MENU_ANONYM;
    }


  }





}
