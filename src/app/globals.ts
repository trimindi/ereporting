/**
 * CONFIG BACKEND ENDPOINT
 */
export const server = "http://103.15.226.134:8000/";
// export const server = "http://localhost:8000/";
export const auth = server + "auth";
export const account = server + "api/account/";
export const accountbb = server + "api/accountbb/";
export const jenis = server + "api/jenis/";
export const kelompok = server + "api/kelompok/";

export const kabupaten = server + "api/wilayah/kabupaten/";
export const kecamatan = server + "api/wilayah/kecamatan/";
export const regional = server + "api/wilayah/regional/";
export const propinsi = server + "api/wilayah/propinsi/";
export const kelurahan = server + "api/wilayah/kelurahan/";
export const kodepos = server + "api/wilayah/kodepos/";

export const iaccount = server + "api/instance/master/account";
export const ianggota = server + "api/instance/master/anggota";
export const ijensisimp = server + "api/instance/jenis/simpanan";
export const isimp = server + "api/instance/master/simpanan";
export const ijenispiut = server + "api/instance/jenis/piutang";
export const ipiut = server + "api/instance/master/piutang";
export const ijenishut = server + "api/instance/jenis/hutang";
export const ihut = server + "api/instance/master/hutang";
export const ijenistab = server + "api/instance/jenis/tabungan";
export const itab = server + "api/instance/master/tabungan";
export const ijenisinv = server + "api/instance/jenis/inventaris";
export const iinv = server + "api/instance/master/inventaris";
export const ijenisdeposito = server + "api/instance/jenis/deposito";
export const ideposito = server + "api/instance/master/deposito";
export const ineraca = server + "api/instance/master/rekap";






