export const ADMIN_MENU = [
  {
    path: '',
    children: [
      {
        path: 'administrator',
        data: {
          menu: {
            title: 'Administrator',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        },
        children: [
          {
            path: '',
            data: {
              menu: {
                title: 'User Profile',
              }
            },
            children: [
              {
                path: 'create',
                data: {
                  menu: {
                    title: 'Create',
                  }
                }
              },
              {
                path: 'update',
                data: {
                  menu: {
                    title: 'Update',
                  }
                }
              },
              {
                path: 'delete',
                data: {
                  menu: {
                    title: 'Delete',
                  }
                }
              },
              {
                path: 'approve',
                data: {
                  menu: {
                    title: 'Approve',
                  }
                }
              }
            ]
          },
          {
            path: 'log',
            data: {
              menu: {
                title: 'Log',
              }
            },
            children: [
              {
                path: 'monitoring',
                data: {
                  menu: {
                    title: 'Monitoring',
                  }
                }
              },
              {
                path: 'backup',
                data: {
                  menu: {
                    title: 'Backup',
                  }
                }
              },
              {
                path: 'clear',
                data: {
                  menu: {
                    title: 'Clear',
                  }
                }
              }
            ]
          }
        ]
      },
      {
        path: 'setting',
        data: {
          menu: {
            title: 'Setting',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        },
        children: [
          {
            path: 'wilayah',
            data: {
              menu: {
                title: 'Wilayah',
              }
            },
            children: [
              {
                path: 'regional',
                data: {
                  menu: {
                    title: 'Regional',
                  }
                }
              },
              {
                path: 'provinsi',
                data: {
                  menu: {
                    title: 'Provinsi',
                  }
                }
              },
              {
                path: 'kabupaten',
                data: {
                  menu: {
                    title: 'Kabupaten',
                  }
                }
              },
              {
                path: 'kecamatan',
                data: {
                  menu: {
                    title: 'Kecamatan',
                  }
                }
              },
              {
                path: 'kelurahan',
                data: {
                  menu: {
                    title: 'Kelurahan / Desa',
                  }
                }
              },
              {
                path: 'kodepos',
                data: {
                  menu: {
                    title: 'Kode Pos',
                  }
                }
              }
            ]
          },
          {
            path: 'dinas',
            data: {
              menu: {
                title: 'Wilayah Dinas',
              }
            },
            children: [
              {
                path: 'provinsi',
                data: {
                  menu: {
                    title: 'Provinsi',
                  }
                }
              },
              {
                path: 'kabupaten',
                data: {
                  menu: {
                    title: 'Kabupaten',
                  }
                }
              }
            ]
          }
        ]
      },
      {
        path: 'parameter',
        data: {
          menu: {
            title: 'Parameter',
            icon: 'ion-edit',
            selected: false,
            expanded: false,
            order: 100,
          }
        },
        children: [
          {
            path: 'jenis',
            data: {
              menu: {
                title: 'Jenis Koperasi',
              }
            }
          }
        ]
      },
      {
        path: '',
        data: {
          menu: {
            title: 'Taksonomi',
            icon: 'ion-stats-bars',
            selected: false,
            expanded: false,
            order: 200,
          }
        },
        children: [
          {
            path: 'taksonomi',
            data: {
              menu: {
                title: 'COA',
              }
            },
            children: [
              {
                path: 'jenis',
                data: {
                  menu: {
                    title: 'Jenis Rekening',
                  }
                }
              },
              {
                path: 'kelompok',
                data: {
                  menu: {
                    title: 'Kelompok Rekening',
                  }
                }
              },
              {
                path: 'bukubesar',
                data: {
                  menu: {
                    title: 'Buku Besar',
                  }
                }
              },
              {
                path: 'subbukubesar',
                data: {
                  menu: {
                    title: 'Sub Buku Besar',
                  }
                }
              }
            ]
          },
          {
            path: 'from',
            data: {
              menu: {
                title: 'FORM Laporan',
              }
            },
            children: [
              {
                path: '1',
                data: {
                  menu: {
                    title: 'FORM 01',
                  }
                }
              },
              {
                path: '2',
                data: {
                  menu: {
                    title: 'FORM 02',
                  }
                }
              },
              {
                path: '3',
                data: {
                  menu: {
                    title: 'FORM 03',
                  }
                }
              },
              {
                path: '4',
                data: {
                  menu: {
                    title: 'FORM 04',
                  }
                }
              },
              {
                path: '5',
                data: {
                  menu: {
                    title: 'FORM 05',
                  }
                }
              },
              {
                path: '6',
                data: {
                  menu: {
                    title: 'FORM 06',
                  }
                }
              }
            ]
          }
        ]
      },
      {
        path: 'laporan',
        data: {
          menu: {
            title: 'Laporan',
            icon: 'ion-android-laptop',
            selected: false,
            expanded: false,
            order: 300,
          }
        },
        children: [
          {
            path: 'instance',
            data: {
              menu: {
                title: 'Create Instance',
              }
            }
          },
          {
            path: 'verify',
            data: {
              menu: {
                title: 'Verifikasi',
              }
            }
          },
          {
            path: 'export',
            data: {
              menu: {
                title: 'Export',
              }
            }
          }
        ]
      },
      {
        path: 'monitoring',
        data: {
          menu: {
            title: 'Monitoring & Evaluasi',
            icon: 'ion-compose',
            selected: false,
            expanded: false,
            order: 400,
          }
        },
        children: [
          {
            path: 'tables',
            data: {
              menu: {
                title: 'Mode Table',
              }
            }
          },
          {
            path: 'maps',
            data: {
              menu: {
                title: 'Mode Peta',
              }
            }
          }
        ]
      }
    ]
  }
];



// export const PAGES_MENU = [
//   {
//     path: 'pages',
//     children: [
//       {
//         path: 'dashboard',
//         data: {
//           menu: {
//             title: 'Dashboard',
//             icon: 'ion-android-home',
//             selected: false,
//             expanded: false,
//             order: 0
//           }
//         }
//       },
//       {
//         path: 'editors',
//         data: {
//           menu: {
//             title: 'Editors',
//             icon: 'ion-edit',
//             selected: false,
//             expanded: false,
//             order: 100,
//           }
//         },
//         children: [
//           {
//             path: 'ckeditor',
//             data: {
//               menu: {
//                 title: 'CKEditor',
//               }
//             }
//           }
//         ]
//       },
//       {
//         path: 'charts',
//         data: {
//           menu: {
//             title: 'Charts',
//             icon: 'ion-stats-bars',
//             selected: false,
//             expanded: false,
//             order: 200,
//           }
//         },
//         children: [
//           {
//             path: 'chartist-js',
//             data: {
//               menu: {
//                 title: 'Chartist.Js',
//               }
//             }
//           }
//         ]
//       },
//       {
//         path: 'ui',
//         data: {
//           menu: {
//             title: 'UI Features',
//             icon: 'ion-android-laptop',
//             selected: false,
//             expanded: false,
//             order: 300,
//           }
//         },
//         children: [
//           {
//             path: 'typography',
//             data: {
//               menu: {
//                 title: 'Typography',
//               }
//             }
//           },
//           {
//             path: 'buttons',
//             data: {
//               menu: {
//                 title: 'Buttons',
//               }
//             }
//           },
//           {
//             path: 'icons',
//             data: {
//               menu: {
//                 title: 'Icons',
//               }
//             }
//           },
//           {
//             path: 'modals',
//             data: {
//               menu: {
//                 title: 'Modals',
//               }
//             }
//           },
//           {
//             path: 'grid',
//             data: {
//               menu: {
//                 title: 'Grid',
//               }
//             }
//           },
//         ]
//       },
//       {
//         path: 'forms',
//         data: {
//           menu: {
//             title: 'Form Elements',
//             icon: 'ion-compose',
//             selected: false,
//             expanded: false,
//             order: 400,
//           }
//         },
//         children: [
//           {
//             path: 'inputs',
//             data: {
//               menu: {
//                 title: 'Form Inputs',
//               }
//             }
//           },
//           {
//             path: 'layouts',
//             data: {
//               menu: {
//                 title: 'Form Layouts',
//               }
//             }
//           }
//         ]
//       },
//       {
//         path: 'tables',
//         data: {
//           menu: {
//             title: 'Tables',
//             icon: 'ion-grid',
//             selected: false,
//             expanded: false,
//             order: 500,
//           }
//         },
//         children: [
//           {
//             path: 'basictables',
//             data: {
//               menu: {
//                 title: 'Basic Tables',
//               }
//             }
//           },
//           {
//             path: 'smarttables',
//             data: {
//               menu: {
//                 title: 'Smart Tables',
//               }
//             }
//           }
//         ]
//       },
//       {
//         path: 'maps',
//         data: {
//           menu: {
//             title: 'Maps',
//             icon: 'ion-ios-location-outline',
//             selected: false,
//             expanded: false,
//             order: 600,
//           }
//         },
//         children: [
//           {
//             path: 'googlemaps',
//             data: {
//               menu: {
//                 title: 'Google Maps',
//               }
//             }
//           },
//           {
//             path: 'leafletmaps',
//             data: {
//               menu: {
//                 title: 'Leaflet Maps',
//               }
//             }
//           },
//           {
//             path: 'bubblemaps',
//             data: {
//               menu: {
//                 title: 'Bubble Maps',
//               }
//             }
//           },
//           {
//             path: 'linemaps',
//             data: {
//               menu: {
//                 title: 'Line Maps',
//               }
//             }
//           }
//         ]
//       },
//       {
//         path: '',
//         data: {
//           menu: {
//             title: 'Pages',
//             icon: 'ion-document',
//             selected: false,
//             expanded: false,
//             order: 650,
//           }
//         },
//         children: [
//           {
//             path: ['/login'],
//             data: {
//               menu: {
//                 title: 'Login'
//               }
//             }
//           },
//           {
//             path: ['/register'],
//             data: {
//               menu: {
//                 title: 'Register'
//               }
//             }
//           }
//         ]
//       },
//       {
//         path: 'master',
//         data: {
//           menu: {
//             title: 'Master',
//             icon: 'ion-grid',
//             selected: false,
//             expanded: false,
//             order: 690,
//           }
//         },
//         children: [
//           {
//             path: 'pelanggan',
//             data: {
//               menu: {
//                 title: 'Pelanggan',
//               }
//             }
//           },
//           {
//             path: 'user',
//             data: {
//               menu: {
//                 title: 'User',
//               }
//             }
//           }
//         ]
//       },
//       {
//         path: '',
//         data: {
//           menu: {
//             title: 'Menu Level 1',
//             icon: 'ion-ios-more',
//             selected: false,
//             expanded: false,
//             order: 700,
//           }
//         },
//         children: [
//           {
//             path: '',
//             data: {
//               menu: {
//                 title: 'Menu Level 1.1',
//                 url: '#'
//               }
//             }
//           },
//           {
//             path: '',
//             data: {
//               menu: {
//                 title: 'Menu Level 1.2',
//                 url: '#'
//               }
//             },
//             children: [
//               {
//                 path: '',
//                 data: {
//                   menu: {
//                     title: 'Menu Level 1.2.1',
//                     url: '#'
//                   }
//                 }
//               }
//             ]
//           }
//         ]
//       }
//     ]
//   }
// ];
