import {Component} from '@angular/core';

@Component({
  selector: 'taksonomi',
  styles: [],
  template: `<router-outlet></router-outlet>`
})
export class TaksonomiComponent {

  constructor() {
  }
}
