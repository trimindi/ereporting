import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { routing } from './taksonomi.routing';
import { TaksonomiComponent } from './taksonomi.component';

import { JenisComponent,JenisService } from './components/coa/jenisrek/index';
import { KelompokComponent,KelompokService } from './components/coa/kelompokrek/';
import { BukuBesarComponent,BukuBesarService } from './components/coa/bukubesar/';
import { SubBukuBesarComponent,SubBukuBesarService } from './components/coa/subbukubesar/';
import {AuthService} from "../login/login.service";
import {Ng2SmartTableModule} from "ng2-smart-table";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgaModule,
        Ng2SmartTableModule,
        routing
    ],
    declarations: [
        TaksonomiComponent,
        JenisComponent,
        KelompokComponent,
        BukuBesarComponent,
        SubBukuBesarComponent
    ],
    providers: [
        AuthService,
        JenisService,
        KelompokService,
        BukuBesarService,
        SubBukuBesarService
    ]
})
export class TaksonomiModule { }
