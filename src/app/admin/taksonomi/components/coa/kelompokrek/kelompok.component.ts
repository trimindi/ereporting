import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { KelompokService } from './kelompok.service';
import { JenisService } from '../jenisrek/jenis.service';
import { Jenis } from '../jenisrek/jenis';
import { Kelompok } from './kelompok'
import {Router} from "@angular/router";
import {LocalDataSource} from "ng2-smart-table";

@Component({
    selector: 'kelompok',
    encapsulation: ViewEncapsulation.None,
    template: require('./kelompok.component.html'),
})
export class KelompokComponent implements OnInit {

  c : Kelompok;
  action : string;
  constructor(private _route : Router,private _kelompokService : KelompokService) {

  }
  ngOnInit() {
    this.start();
  }
  cancel() : void{
    this.c = null;
  }

  start(){

    this._kelompokService.getKelompok()
      .subscribe(res =>
          this.source.load(res),
        err => this.error(err));

  }


  error(err){
    if(err.status == 401){
      localStorage.removeItem("user");
      this._route.navigate(['login']);
    }else {
      console.error(err);
    }
  }


  simpan(){
    if(this.action == 'Edit'){
      this._kelompokService.editKelompok(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }else{
      this._kelompokService.editKelompok(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }
  }
  edit(event){
    this.action = "Edit";
    this.c = <Kelompok>event.data;
  }
  create(event){
    this.action = "Create";
    this.c = new Kelompok();
  }

  delete(event){
    this._kelompokService.deleteKelompok(<Kelompok>event.data).subscribe(res => {
      this.start();
    },err=> {});
  }


  query: string = '';

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Actions',
      add: true,
      edit: true,
      delete: true
    },
    columns: {
      accjenis :{
        title: 'ACC JENIS',
        type: 'string'
      },
      acckel:  {
        title: 'ACC KEL',
        type: 'string'
      },
      accbb: {
        title: 'ACCBB',
        type: 'string'
      },
      kelompok: {
        title: 'BUKU BESAR',
        type: 'string'
      },
      gol: {
        title: 'GOLONGAN',
        type: 'string'
      },
      subgol: {
        title: 'SUB GOLONGAN',
        type: 'string'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();


}
