import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { Kelompok } from './kelompok';
import conf = require('./../../../../../globals.ts');
import {AuthService} from "../../../../login/login.service";

@Injectable()
export class KelompokService {

    constructor(
        private _auth : AuthService,
        private http: Http) {
    }
    getKelompok(): Observable<Kelompok[]> {
        return this.http.get(conf.kelompok , this.Auth())
            .map((response: Response) => response.json());
    }
    editKelompok(k : Kelompok): Observable<any> {
      return this.http.put( conf.kelompok ,JSON.stringify(k), this.Auth())
        .map((response: Response) => response.json());
    }


  createKelompok(b : Kelompok): Observable<any> {
    return this.http.post(conf.kelompok,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  deleteKelompok(b : Kelompok): Observable<any> {
    return this.http.post(conf.kelompok+'destroy',JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }


  private Auth() {
        let headers = new Headers({ 'Authorization': this._auth.getToken(), 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }
}
