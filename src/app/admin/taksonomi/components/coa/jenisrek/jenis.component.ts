import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { JenisService } from './jenis.service';
import { Jenis } from './jenis'
import {Router} from "@angular/router";
import {LocalDataSource} from "ng2-smart-table";

@Component({
    selector: 'jenis',
    encapsulation: ViewEncapsulation.None,
    template: require('./jenis.component.html'),
})
export class JenisComponent implements OnInit {

  c : Jenis;
  action : string;
  constructor(private _route : Router,private _jenisService : JenisService) {

  }
  ngOnInit() {
    this.start();
  }
  cancel() : void{
    this.c = null;
  }

  start(){

    this._jenisService.getJenis()
      .subscribe(res =>
          this.source.load(res),
        err => this.error(err));

  }


  error(err){
    if(err.status == 401){
      localStorage.removeItem("user");
      this._route.navigate(['login']);
    }else {
      console.error(err);
    }
  }


  simpan(){
    if(this.action == 'Edit'){
      this._jenisService.editJenis(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }else{
      this._jenisService.editJenis(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }
  }
  edit(event){
    this.action = "Edit";
    this.c = <Jenis>event.data;
  }



  query: string = '';
  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: true,
      delete: false
    },
    columns: {
      accjenis:  {
        title: 'ACC JENIS',
        type: 'string'
      },
      jenis: {
        title: 'JENIS',
        type: 'string'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();


}
