import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { Jenis } from './jenis';
import conf = require('./../../../../../globals.ts');
import {AuthService} from "../../../../login/login.service";



@Injectable()
export class JenisService {


    constructor(
      private _auth : AuthService,
        private http: Http) {
    }

    getJenis(): Observable<Jenis[]> {
        return this.http.get(conf.jenis, this.Auth())
            .map((response: Response) => response.json());
    }
    editJenis(j:Jenis): Observable<Jenis> {
        return this.http.put(conf.jenis,JSON.stringify(j), this.Auth())
            .map((response: Response) => response.json());
    }
    createJenis(b : Jenis): Observable<any> {
      return this.http.post(conf.kelompok,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }
    deleteJenis(b : Jenis): Observable<any> {
      return this.http.post(conf.kelompok+'destroy',JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }

    private Auth() {
        let headers = new Headers({ 'Authorization': this._auth.getToken() , 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }
}
