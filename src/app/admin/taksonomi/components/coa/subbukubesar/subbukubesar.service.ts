import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { SubBukuBesar } from './subbukubesar';
import conf = require('./../../../../../globals.ts');
import {AuthService} from "../../../../login/login.service";

@Injectable()
export class SubBukuBesarService {
    constructor(
      private _auth : AuthService,
        private http: Http) {
    }

    getSubBukuBesar(): Observable<SubBukuBesar[]> {
        return this.http.get(conf.account , this.Auth())
            .map((response: Response) => response.json());
    }
    editSubBukuBesar(b : SubBukuBesar): Observable<any> {
      return this.http.put(conf.account,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }

    createSubBukuBesar(b : SubBukuBesar): Observable<any> {
      return this.http.post(conf.account,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }
    deleteSubBukuBesar(b : SubBukuBesar): Observable<any> {
      return this.http.post(conf.account+'destroy',JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }


    private Auth() {
        let headers = new Headers({ 'Authorization': this._auth.getToken() , 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }
}
