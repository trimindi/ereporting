import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import {LocalDataSource} from "ng2-smart-table";
import {Router} from "@angular/router";
import {SubBukuBesarService} from "./subbukubesar.service";
import {SubBukuBesar} from "./subbukubesar";


@Component({
    selector: 'subbukubesar',
    encapsulation: ViewEncapsulation.None,
    template: require('./subbukubesar.component.html'),
})
export class SubBukuBesarComponent implements OnInit {
  c : SubBukuBesar;
  action : string;
  constructor(private _route : Router,private _subbukuBesarService : SubBukuBesarService) {

  }
  ngOnInit() {
    this.start();
  }
  cancel() : void{
    this.c = null;
  }

  start(){

    this._subbukuBesarService.getSubBukuBesar()
      .subscribe(res =>
          this.source.load(res),
        err => this.error(err));

  }


  error(err){
    if(err.status == 401){
      localStorage.removeItem("user");
      this._route.navigate(['login']);
    }else {
      console.error(err);
    }
  }


  simpan(){
    if(this.action == 'Edit'){
      this._subbukuBesarService.editSubBukuBesar(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }else{
      this._subbukuBesarService.editSubBukuBesar(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }
  }
  edit(event){
    this.action = "Edit";
    this.c = <SubBukuBesar>event.data;
  }
  create(event){
    this.action = "Create";
    this.c = new SubBukuBesar();
  }

  delete(event){
    this._subbukuBesarService.deleteSubBukuBesar(<SubBukuBesar>event.data).subscribe(res => {
      this.start();
    },err=> {});
  }


  query: string = '';

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Actions',
      add: true,
      edit: true,
      delete: true
    },
    columns: {

      accbb: {
        title: 'ACCBB',
        type: 'string'
      },
      acc: {
        title: 'ACC',
        type: 'string'
      },
      keterangan: {
        title: 'KETERANGAN',
        type: 'string'
      },
      golongan: {
        title: 'GOLONGAN',
        type: 'string'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();
}
