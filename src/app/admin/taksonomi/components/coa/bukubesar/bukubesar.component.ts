import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import {BukuBesar} from "./bukubesar";
import {BukuBesarService} from "./bukubesar.service";
import {Router} from "@angular/router";
import {LocalDataSource} from "ng2-smart-table";


@Component({
    selector: 'bukubesar',
    encapsulation: ViewEncapsulation.None,
    template: require('./bukubesar.component.html'),
})
export class BukuBesarComponent implements OnInit {
  c : BukuBesar;
  action : string;
  constructor(private _route : Router,private _bukubesarService : BukuBesarService) {

  }
  ngOnInit() {
    this.start();
  }
  cancel() : void{
    this.c = null;
  }

  start(){

    this._bukubesarService.getBukubesar()
      .subscribe(res =>
          this.source.load(res),
        err => this.error(err));

  }


  error(err){
    if(err.status == 401){
      localStorage.removeItem("user");
      this._route.navigate(['login']);
    }else {
      console.error(err);
    }
  }


  simpan(){
    if(this.action == 'Edit'){
      this._bukubesarService.editBukubesar(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }else{
      this._bukubesarService.createBukuBesar(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }
  }
  edit(event){
    this.action = "Edit";
    this.c = <BukuBesar>event.data;
  }
  create(event){
    this.action = "Create";
    this.c = new BukuBesar();
  }

  delete(event){
    this._bukubesarService.deleteBukuBesar(<BukuBesar>event.data).subscribe(res => {
      this.start();
    },err=> {});
  }


  query: string = '';

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Actions',
      add: true,
      edit: true,
      delete: true
    },
    columns: {
      acckel:  {
        title: 'KEL',
        type: 'string'
      },
      accbb: {
        title: 'ACCBB',
        type: 'string'
      },
      bukubesar: {
        title: 'BUKU BESAR',
        type: 'string'
      },
      kategori: {
        title: 'KATEGORI',
        type: 'string'
      },
      golongan: {
        title: 'GOLONGAN',
        type: 'string'
      },
      resiko: {
        title: 'RESIKO',
        type: 'string'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();


}
