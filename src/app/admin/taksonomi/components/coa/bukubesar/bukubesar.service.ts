import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { BukuBesar } from './bukubesar';
import conf = require('./../../../../../globals');
import {AuthService} from "../../../../login/login.service";



@Injectable()
export class BukuBesarService {
    constructor(
        private _auth : AuthService,
        private http: Http) {
    }

    getBukubesar(): Observable<BukuBesar[]> {
        return this.http.get(conf.accountbb, this.Auth())
            .map((response: Response) => response.json());
    }

    editBukubesar(b : BukuBesar): Observable<BukuBesar[]> {
      return this.http.put(conf.accountbb, JSON.stringify(b) ,this.Auth())
        .map((response: Response) => response.json());
    }

    createBukuBesar(b : BukuBesar): Observable<any> {
      return this.http.post(conf.accountbb,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }
    deleteBukuBesar(b : BukuBesar): Observable<any> {
      return this.http.post(conf.accountbb+'destroy',JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }

    private Auth() {
        let headers = new Headers({ 'Authorization': this._auth.getToken(), 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }
}
