import { Routes, RouterModule }  from '@angular/router';

import { TaksonomiComponent } from './taksonomi.component';
import { JenisComponent } from './components/coa/jenisrek/index';
import { KelompokComponent } from './components/coa/kelompokrek/';
import { BukuBesarComponent } from './components/coa/bukubesar/';
import { SubBukuBesarComponent } from './components/coa/subbukubesar/';


const routes: Routes = [
  {
    path: '',
    component: TaksonomiComponent,
    children: [
      { path: 'jenis', component: JenisComponent },
      { path: 'kelompok', component: KelompokComponent },
      { path: 'subbukubesar', component: SubBukuBesarComponent },
      { path: 'bukubesar', component: BukuBesarComponent }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
