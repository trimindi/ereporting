import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AuthGuard } from '../_guard/auth.guard';
import {ModuleWithProviders} from "@angular/core";


const routes: Routes = [
  {
    path: 'login',
    loadChildren: 'app/admin/login/login.module#LoginModule'
  },
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: 'app/admin/dashboard/dashboard.module#DashboardModule' },
      { path: 'taksonomi', loadChildren: 'app/admin/taksonomi/taksonomi.module#TaksonomiModule' },
      { path: 'laporan', loadChildren: 'app/admin/laporan/laporan.module#LaporanModule'},
      { path: 'setting', loadChildren: 'app/admin/setting/setting.module#SettingModule'}
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
