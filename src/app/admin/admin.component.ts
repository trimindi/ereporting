import {Component, ViewEncapsulation} from '@angular/core';
import {CookieService} from "angular2-cookie/services/cookies.service";
import {Route, Router} from "@angular/router";


@Component({
  selector: 'admin',
  template: `
    <ba-sidebar></ba-sidebar>
    <ba-page-top (logoutAction)="logout()"></ba-page-top>
    <div class="al-main">
      <div class="al-content">
        <ba-content-top></ba-content-top>
        <router-outlet></router-outlet>
      </div>
    </div>
    <footer class="al-footer clearfix">
    </footer>
    <ba-back-top position="200"></ba-back-top>
    `
})
export class AdminComponent {

    constructor(private _cookieService : CookieService,private _route:Router){

    }
    logout(){

      this._cookieService.removeAll();
      this._route.navigate(['login']);

    }
}
