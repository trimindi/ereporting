import { Routes, RouterModule }  from '@angular/router';

import { SettingComponent } from './setting.component';
import {KabupatenComponent} from "./wilayah/kabupaten/kabupaten.component";
import {KecamatanComponent} from "./wilayah/kecamatan/kecamatan.component";
import {KelurahanComponent} from "./wilayah/kelurahan/kelurahan.component";
import {KodeposComponent} from "./wilayah/kodepos/kodepos.component";
import {PropinsiComponent} from "./wilayah/propinsi/propinsi.component";
import {RegionalComponent} from "./wilayah/regional/regional.component";

const routes: Routes = [
  {
    path: 'wilayah',
    component: SettingComponent,
    children: [
      { path: 'kabupaten', component: KabupatenComponent },
      { path: 'kecamatan', component: KecamatanComponent },
      { path: 'kelurahan', component: KelurahanComponent },
      { path: 'kodepos', component: KodeposComponent },
      { path: 'provinsi', component: PropinsiComponent },
      { path: 'regional', component: RegionalComponent }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
