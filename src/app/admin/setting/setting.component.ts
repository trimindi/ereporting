import {Component} from '@angular/core';

@Component({
  selector: 'setting',
  styles: [],
  template: `<router-outlet></router-outlet>`
})
export class SettingComponent {

  constructor() {
  }

}
