import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { routing } from './setting.routing';
import {Ng2SmartTableModule} from "ng2-smart-table";
import {RegionalComponent} from "./wilayah/regional/regional.component";
import {PropinsiComponent} from "./wilayah/propinsi/propinsi.component";
import {KodeposComponent} from "./wilayah/kodepos/kodepos.component";
import {KelurahanService} from "./wilayah/kelurahan/kelurahan.service";
import {KelurahanComponent} from "./wilayah/kelurahan/kelurahan.component";
import {KecamatanComponent} from "./wilayah/kecamatan/kecamatan.component";
import {KecamatanService} from "./wilayah/kecamatan/kecamatan.service";
import {KabupatenComponent} from "./wilayah/kabupaten/kabupaten.component";
import {KabupatenService} from "./wilayah/kabupaten/kabupaten.service";
import {KodeposService} from "./wilayah/kodepos/kodepos.service";
import {PropinsiService} from "./wilayah/propinsi/propinsi.service";
import {RegionalService} from "./wilayah/regional/regional.service";
import {SettingComponent} from "./setting.component";
import {AuthService} from "../login/login.service";
import {ModalModule} from "angular2-modal";
import {BootstrapModalModule} from "angular2-modal/plugins/bootstrap";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    Ng2SmartTableModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    routing
  ],
  declarations: [
    SettingComponent,
    KabupatenComponent,
    KecamatanComponent,
    KelurahanComponent,
    KodeposComponent,
    PropinsiComponent,
    RegionalComponent
  ],
  providers: [
    AuthService,
    KabupatenService,
    KecamatanService,
    KelurahanService,
    KodeposService,
    PropinsiService,
    RegionalService

  ]
})
export class SettingModule { }
