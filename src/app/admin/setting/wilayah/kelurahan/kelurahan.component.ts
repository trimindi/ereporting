import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import {Router} from "@angular/router";
import {LocalDataSource} from "ng2-smart-table";
import {KelurahanService} from "./kelurahan.service";
import {Kelurahan} from "./Kelurahan";



@Component({
    selector: 'kelurahan',
    encapsulation: ViewEncapsulation.None,
    template: require('./kelurahan.component.html'),
})
export class KelurahanComponent implements OnInit {
    c : Kelurahan;
    constructor(private _route : Router,private _kelurahanService : KelurahanService) {

    }
    ngOnInit() {
        this.start();
    }
    cancel() : void{
      this.c = null;
    }

    start(){

      this._kelurahanService.getKelurahan()
        .subscribe(res =>
          this.source.load(res),
          err => this.error(err));

    }


    error(err){
      if(err.status == 401){
        localStorage.removeItem("user");
        this._route.navigate(['login']);
      }else {
        console.error(err);
      }
    }


  edit(event){
    this.c = <Kelurahan>event.data;
  }
  create(event){
    this.c = new Kelurahan();
  }

  delete(event){
    this._kelurahanService.deleteKelurahan(<Kelurahan>event.data).subscribe(res => {
      this.start();
    },err=> {});
  }


  query: string = '';

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Actions',
      add: true,
      edit: true,
      delete: true
    },
    columns: {
      id_kel:  {
        title: 'ID',
        type: 'string'
      },
      nama_kelurahan: {
        title: 'ID KELURAHAN',
        type: 'string'
      },
      id_kec: {
        title: 'NAMA KECAMATAN',
        type: 'string'
      },
      kodepos: {
        title: 'KODEPOS',
        type: 'string'
      },
      longitude: {
        title: 'LONGITUDE',
        type: 'string'
      },
      lattitude: {
        title: 'LATTITUDE',
        type: 'string'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();

}
