import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import  conf = require('./../../../../globals.ts');
import {Kelurahan} from "./Kelurahan";
import {AuthService} from "../../../login/login.service";



@Injectable()
export class KelurahanService {
    constructor(
        private _auth : AuthService,
        private http: Http) {
    }

    getKelurahan(): Observable<Kelurahan[]> {
        return this.http.get(conf.kelurahan, this.Auth())
            .map((response: Response) => response.json());
    }

    editKelurahan(b : Kelurahan): Observable<Kelurahan[]> {
      return this.http.put(conf.kelurahan, JSON.stringify(b) ,this.Auth())
        .map((response: Response) => response.json());
    }

    createKelurahan(b : Kelurahan): Observable<any> {
      return this.http.post(conf.kelurahan,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }
    deleteKelurahan(b : Kelurahan): Observable<any> {
      return this.http.post(conf.kelurahan+'destroy',JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }

    private Auth() {
        let headers = new Headers({ 'Authorization': this._auth.getToken(), 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }
}
