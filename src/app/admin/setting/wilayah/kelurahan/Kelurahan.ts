export class Kelurahan {
  id_kel: string;
  nama_kelurahan: string;
  id_kec: string;
  kodepos?: any;
  longitude: string;
  lattitude: string;
}
