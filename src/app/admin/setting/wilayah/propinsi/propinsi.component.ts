import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import {Router} from "@angular/router";
import {Kabupaten} from "./Kabupaten";
import {LocalDataSource} from "ng2-smart-table";
import {KabupatenService} from "./kabupaten.service";
import {Propinsi} from "./Propinsi";
import {PropinsiService} from "./propinsi.service";


@Component({
    selector: 'porpinsi',
    encapsulation: ViewEncapsulation.None,
    template: require('./propinsi.component.html'),
})
export class PropinsiComponent implements OnInit {
    action : string;
    c : Propinsi;

    constructor(private _route : Router,private _propinsiService : PropinsiService) {

    }
    ngOnInit() {
        this.start();
    }
    cancel() : void{
      this.c = null;
    }

    start(){

      this._propinsiService.getPropinsi()
        .subscribe(res =>
          this.source.load(res),
          err => this.error(err));

    }


    error(err){
      if(err.status == 401){
        localStorage.removeItem("user");
        this._route.navigate(['login']);
      }else {
        console.error(err);
      }
    }

  simpan(){
    if(this.action == 'Edit'){
      this._propinsiService.editPropinsi(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }else{
      this._propinsiService.createPropinsi(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }
  }

  edit(event){
    this.action = "Edit";
    this.c = <Propinsi>event.data;
  }
  create(event){
    this.action = "Create";
    this.c = new Propinsi();
  }

  delete(event){
    this._propinsiService.deletePropinsi(<Propinsi>event.data).subscribe(res => {
      this.start();
    },err=> {});
  }


  query: string = '';

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Actions',
      add: true,
      edit: true,
      delete: true
    },
    columns: {
      idreg:  {
        title: 'ID REGISTRASI',
        type: 'string'
      },
      id_prop: {
        title: 'ID PROPINSI',
        type: 'string'
      },
      nama_propinsi: {
        title: 'NAMA PROPINSI',
        type: 'string'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();

}
