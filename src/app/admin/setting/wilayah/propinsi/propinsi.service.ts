import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import  conf = require('./../../../../globals.ts');
import {Propinsi} from "./Propinsi";
import {AuthService} from "../../../login/login.service";



@Injectable()
export class PropinsiService {
    constructor(
      private _auth : AuthService,
        private http: Http) {
    }

    getPropinsi(): Observable<Propinsi[]> {
        return this.http.get(conf.propinsi, this.Auth())
            .map((response: Response) => response.json());
    }

    editPropinsi(b : Propinsi): Observable<Propinsi[]> {
      return this.http.put(conf.propinsi, JSON.stringify(b) ,this.Auth())
        .map((response: Response) => response.json());
    }

    createPropinsi(b : Propinsi): Observable<any> {
      return this.http.post(conf.propinsi,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }
    deletePropinsi(b : Propinsi): Observable<any> {
      return this.http.post(conf.propinsi+'destroy',JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }

    private Auth() {
        let headers = new Headers({ 'Authorization': this._auth.getToken(), 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }
}
