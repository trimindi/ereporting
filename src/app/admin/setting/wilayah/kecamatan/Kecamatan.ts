export class Kecamatan {
  id_kec: string;
  id_kab: string;
  nama_kecamatan: string;
  kodepos?: any;
  ket?: any;
}
