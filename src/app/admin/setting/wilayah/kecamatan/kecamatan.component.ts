import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import {Router} from "@angular/router";
import {LocalDataSource} from "ng2-smart-table";
import {KecamatanService} from "./kecamatan.service";
import {Kecamatan} from "./Kecamatan";



@Component({
    selector: 'kecamatan',
    encapsulation: ViewEncapsulation.None,
    template: require('./kecamatan.component.html'),
})
export class KecamatanComponent implements OnInit {
    c : Kecamatan;
    action : string;
    constructor(private _route : Router,private _kecamatanService : KecamatanService) {

    }
    ngOnInit() {
        this.start();
    }
    cancel() : void{
      this.c = null;
    }

    start(){

      this._kecamatanService.getKecamatan()
        .subscribe(res =>
          this.source.load(res),
          err => this.error(err));

    }


    error(err){
      if(err.status == 401){
        localStorage.removeItem("user");
        this._route.navigate(['login']);
      }else {
        console.error(err);
      }
    }


  simpan(){
    if(this.action == 'Edit'){
      this._kecamatanService.editKecamatan(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }else{
      this._kecamatanService.createKecamatan(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }
  }
  edit(event){
    this.action = "Edit";
    this.c = <Kecamatan>event.data;
  }
  create(event){
    this.action = "Create";
    this.c = new Kecamatan();
  }

  delete(event){
    this._kecamatanService.deleteKecamatan(<Kecamatan>event.data).subscribe(res => {
      this.start();
    },err=> {});
  }


  query: string = '';

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Actions',
      add: true,
      edit: true,
      delete: true
    },

    columns: {
      id_kec:  {
        title: 'ID',
        type: 'string'
      },
      id_kab: {
        title: 'ID KABUPATEN',
        type: 'string'
      },
      nama_kecamatan: {
        title: 'NAMA KECAMATAN',
        type: 'string'
      },
      kodepos: {
        title: 'KODEPOS',
        type: 'string'
      },
      ket: {
        title: 'KETERANGAN',
        type: 'string'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();

}
