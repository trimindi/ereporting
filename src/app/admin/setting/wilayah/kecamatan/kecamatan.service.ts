import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import  conf = require('./../../../../globals.ts');
import {Kecamatan} from "./Kecamatan";
import {AuthService} from "../../../login/login.service";



@Injectable()
export class KecamatanService {
    constructor(
        private _auth : AuthService,
        private http: Http) {
    }

    getKecamatan(): Observable<Kecamatan[]> {
        return this.http.get(conf.kecamatan, this.Auth())
            .map((response: Response) => response.json());
    }

    editKecamatan(b : Kecamatan): Observable<Kecamatan[]> {
      return this.http.put(conf.kecamatan, JSON.stringify(b) ,this.Auth())
        .map((response: Response) => response.json());
    }

    createKecamatan(b : Kecamatan): Observable<any> {
      return this.http.post(conf.kecamatan,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }
    deleteKecamatan(b : Kecamatan): Observable<any> {
      return this.http.post(conf.kecamatan+'destroy',JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }

    private Auth() {
        let headers = new Headers({ 'Authorization': this._auth.getToken(), 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }
}
