import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import  conf = require('./../../../../globals.ts');
import {Regional} from "./Regional";
import {AuthService} from "../../../login/login.service";



@Injectable()
export class RegionalService {
    constructor(
      private _auth : AuthService,
        private http: Http) {
    }
    getRegional(): Observable<Regional[]> {
        return this.http.get(conf.regional, this.Auth())
            .map((response: Response) => response.json());
    }
    editRegional(b : Regional): Observable<Regional[]> {
      return this.http.put(conf.regional, JSON.stringify(b) ,this.Auth())
        .map((response: Response) => response.json());
    }

    createRegional(b : Regional): Observable<any> {
      return this.http.post(conf.regional,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }
    deleteRegional(b : Regional): Observable<any> {
      return this.http.post(conf.regional+'destroy',JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }

    private Auth() {
        let headers = new Headers({ 'Authorization': this._auth.getToken(), 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }
}
