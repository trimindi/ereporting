export class Regional {
  idreg: string;
  nama_regional: string;
  nama_koordinator: string;
  telepon: string;
  e_mail: string;
}
