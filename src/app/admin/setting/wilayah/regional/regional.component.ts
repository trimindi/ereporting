import { Component, ViewEncapsulation, OnInit } from '@angular/core';


import {Router} from "@angular/router";
import {Kabupaten} from "./Kabupaten";
import {LocalDataSource} from "ng2-smart-table";
import {KabupatenService} from "./kabupaten.service";
import {Regional} from "./Regional";
import {RegionalService} from "./regional.service";


@Component({
    selector: 'regional',
    encapsulation: ViewEncapsulation.None,
    template: require('./regional.component.html'),
})
export class RegionalComponent implements OnInit {
    action : string;
    c : Regional;

    constructor(private _route : Router,private _RegionalService : RegionalService) {
    }
    ngOnInit() {
        this.start();
    }
    cancel() : void{
      this.c = null;
    }

    start(){

      this._RegionalService.getRegional()
        .subscribe(res =>
          this.source.load(res),
          err => this.error(err));

    }


    error(err){
      if(err.status == 401){
        localStorage.removeItem("user");
        this._route.navigate(['login']);
      }else {
        console.error(err);
      }
    }

    simpan(){
      if(this.action == 'Edit'){
        this._RegionalService.editRegional(this.c).subscribe(res => {
          this.start();
        },err=> {console.error(err)});
      }else{
        this._RegionalService.createRegional(this.c).subscribe(res => {
          this.start();
        },err=> {console.error(err)});
      }
    }

    edit(event){
      this.action = 'Edit';
      this.c = <Regional>event.data;
    }


    create(event){
      this.action = 'Create';
      this.c = new Regional();
    }

    delete(event){
      this._RegionalService.deleteRegional(<Regional>event.data).subscribe(res => {
        this.start();
      },err=> {});
    }


  query: string = '';

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Actions',
      add: true,
      edit: true,
      delete: true
    },
    columns: {
      idreg:  {
        title: 'ID REGISTRASI',
        type: 'string'
      },
      nama_regional: {
        title: 'NAMA REGIONAL',
        type: 'string'
      },
      nama_koordinator: {
        title: 'NAMA KORDINATOR',
        type: 'string'
      },
      telepon: {
        title: 'TELEPON',
        type: 'string'
      },
      e_mail: {
        title: 'E-MAIL',
        type: 'string'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();

}
