import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import {Router} from "@angular/router";
import {LocalDataSource} from "ng2-smart-table";
import {Kodepos} from "./Kodepos";
import {KodeposService} from "./kodepos.service";


@Component({
    selector: 'kodepos',
    encapsulation: ViewEncapsulation.None,
    template: require('./kodepos.component.html'),
})
export class KodeposComponent implements OnInit {
    c : Kodepos;
    action :string;
    constructor(private _route : Router,private _kodeposService : KodeposService) {

    }
    ngOnInit() {
        this.start();
    }
    cancel() : void{
      this.c = null;
    }

    start(){

      this._kodeposService.getKodepos()
        .subscribe(res =>
          this.source.load(res),
          err => this.error(err));

    }

    error(err){
      if(err.status == 401){
        localStorage.removeItem("user");
        this._route.navigate(['login']);
      }else {
        console.error(err);
      }
    }

  simpan(){
    if(this.action == 'Edit'){
      this._kodeposService.editKodepos(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }else{
      this._kodeposService.createKodepos(this.c).subscribe(res => {
        this.start();
      },err=> {console.error(err)});
    }
  }

  edit(event){
    this.action = "Edit";
    this.c = <Kodepos>event.data;
  }
  create(event){
    this.action = "Create";
    this.c = new Kodepos();
  }

  delete(event){
    this._kodeposService.deleteKodepos(<Kodepos>event.data).subscribe(res => {
      this.start();
    },err=> {});
  }


  query: string = '';

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Actions',
      add: true,
      edit: true,
      delete: true
    },
    columns: {
      id_kode:  {
        title: 'ID',
        type: 'string'
      },
      id_kel: {
        title: 'ID KELURAHAN',
        type: 'string'
      },
      nama_kelurahan: {
        title: 'NAMA KELURAHAN',
        type: 'string'
      },
      nama_kecamatan: {
        title: 'NAMA KECAMATAN',
        type: 'string'
      },
      res: {
        title: 'RES',
        type: 'string'
      },
      nama_kabupaten: {
        title: 'NAMA KABUPATEN',
        type: 'string'
      },
      nama_propinsi: {
        title: 'NAMA PROPINSI',
        type: 'string'
      },
    }
  };

  source: LocalDataSource = new LocalDataSource();

}
