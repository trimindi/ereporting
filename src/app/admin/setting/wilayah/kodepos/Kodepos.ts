export class Kodepos {
  id_kode: string;
  id_kel?: any;
  nama_kelurahan: string;
  nama_kecamatan: string;
  res: string;
  nama_kabupaten: string;
  nama_propinsi: string;
}
