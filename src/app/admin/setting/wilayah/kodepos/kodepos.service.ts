import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import  conf = require('./../../../../globals.ts');
import {Kodepos} from "./Kodepos";
import {AuthService} from "../../../login/login.service";



@Injectable()
export class KodeposService {
    constructor(
      private _auth : AuthService,
        private http: Http) {
    }

    getKodepos(): Observable<Kodepos[]> {
        return this.http.get(conf.kodepos, this.Auth())
            .map((response: Response) => response.json());
    }

    editKodepos(b : Kodepos): Observable<Kodepos[]> {
      return this.http.put(conf.kodepos, JSON.stringify(b) ,this.Auth())
        .map((response: Response) => response.json());
    }

    createKodepos(b : Kodepos): Observable<any> {
      return this.http.post(conf.kodepos,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }
    deleteKodepos(b : Kodepos): Observable<any> {
      return this.http.post(conf.kodepos+'destroy',JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }

    private Auth() {
        let headers = new Headers({ 'Authorization': this._auth.getToken(), 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }
}
