import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import {Router} from "@angular/router";
import {Kabupaten} from "./Kabupaten";
import {LocalDataSource} from "ng2-smart-table";
import {KabupatenService} from "./kabupaten.service";


@Component({
    selector: 'kabupaten',
    encapsulation: ViewEncapsulation.None,
    template: require('./kabupaten.component.html'),
})
export class KabupatenComponent implements OnInit {
    action : string;
    c : Kabupaten;

    constructor(private _route : Router,private _kabupatenService : KabupatenService) {

    }
    ngOnInit() {
        this.start();
    }
    cancel() : void{
      this.c = null;
    }

    start(){

      this._kabupatenService.getKabupaten()
        .subscribe(res =>
          this.source.load(res),
          err => this.error(err));

    }

    simpan(){
      if(this.action == 'Edit'){
        this._kabupatenService.editKabupaten(this.c).subscribe(res => {
          this.start();
        },err=> {console.error(err)});
      }else{
        this._kabupatenService.createKabupaten(this.c).subscribe(res => {
          this.start();
        },err=> {console.error(err)});
      }
    }

    edit(event){
      this.action = "Edit";
      this.c = <Kabupaten>event.data;
    }
    create(event){
      this.action = "Create";
      this.c = new Kabupaten();
    }

    delete(event){
      this._kabupatenService.deleteKabupaten(<Kabupaten>event.data).subscribe(res => {
        this.start();
      },err=> {});
    }

    error(err){
      if(err.status == 401){
        localStorage.removeItem("user");
        this._route.navigate(['login']);
      }else {
        console.error(err);
      }
    }


  query: string = '';

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Actions',
      add: true,
      edit: true,
      delete: true
    },
    columns: {
      id_prop:  {
        title: 'ID PROP',
        type: 'string'
      },
      id_kab: {
        title: 'ID KAB',
        type: 'string'
      },
      res:{
        title: 'RES',
        type: 'string'
      },
      nama_kabupaten: {
        title: 'NAMA KABUPATEN',
        type: 'string'
      },
      ibukota: {
        title: 'IBUKOTA',
        type: 'string'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();

}
