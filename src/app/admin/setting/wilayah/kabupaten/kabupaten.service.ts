import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { Kabupaten } from './Kabupaten';
import  conf = require('./../../../../globals.ts');
import {AuthService} from "../../../login/login.service";



@Injectable()
export class KabupatenService {
    constructor(
      private _auth : AuthService,
        private http: Http) {
    }

    getKabupaten(): Observable<Kabupaten[]> {
        return this.http.get(conf.kabupaten, this.Auth())
            .map((response: Response) => response.json());
    }

    editKabupaten(b : Kabupaten): Observable<Kabupaten[]> {
      return this.http.put(conf.kabupaten, JSON.stringify(b) ,this.Auth())
        .map((response: Response) => response.json());
    }

    createKabupaten(b : Kabupaten): Observable<any> {
      return this.http.post(conf.kabupaten,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }
    deleteKabupaten(b : Kabupaten): Observable<any> {
      return this.http.post(conf.kabupaten+'destroy',JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
    }

    private Auth() {
        let headers = new Headers({ 'Authorization': this._auth.getToken(), 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }
}
