import { Routes, RouterModule }  from '@angular/router';

import { LaporanComponent } from './laporan.component';
import {InstanceComponent} from "./components/instance/instance.component";



const routes: Routes = [
  {
    path: '',
    component: LaporanComponent,
    children: [
      { path: 'instance', component: InstanceComponent },
    ]
  }
];

export const routing = RouterModule.forChild(routes);
