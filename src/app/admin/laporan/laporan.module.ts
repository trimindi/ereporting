import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { routing } from './laporan.routing';
import { LaporanComponent } from './laporan.component';
import { InstanceComponent } from "./components/instance/instance.component";
import {Ng2SmartTableModule} from "ng2-smart-table";
import {InstanceService} from "./components/instance/instance.service";
import {AuthService} from "../login/login.service";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    Ng2SmartTableModule,
    routing
  ],
  declarations: [
    LaporanComponent,
    InstanceComponent

  ],
  providers: [
    InstanceService,
    AuthService
  ]
})
export class LaporanModule { }
