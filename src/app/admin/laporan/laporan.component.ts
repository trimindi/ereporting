import {Component} from '@angular/core';

@Component({
  selector: 'laporan',
  styles: [],
  template: `<router-outlet></router-outlet>`
})
export class LaporanComponent {

  constructor() {
  }
}
