import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import conf = require('./../../../../globals');
import {AuthService} from "../../../login/login.service";
declare var alasql: any;


@Injectable()
export class InstanceService {
  public alasql: any;

  constructor(
    private _auth : AuthService,
    private http: Http) {
  }

  saveAccount(b : any[]): Observable<any> {
    return this.http.post(conf.iaccount,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveAnggota(b : any[]): Observable<any> {
    return this.http.post(conf.ianggota,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveJenisSimpanan(b : any[]): Observable<any> {
    return this.http.post(conf.ijensisimp,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveSimpanan(b : any[]): Observable<any> {
    return this.http.post(conf.isimp,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());

  }
  saveJenisPiutang(b : any[]): Observable<any> {
    return this.http.post(conf.ijenispiut,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  savePiutang(b : any[]): Observable<any> {
    return this.http.post(conf.ipiut,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveJenisTabungan(b : any[]): Observable<any> {
  return this.http.post(conf.ijenistab,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveTabungan(b : any[]): Observable<any> {
    return this.http.post(conf.itab,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveJenisInventaris(b : any[]): Observable<any> {
    return this.http.post(conf.ijenisinv,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveInventaris(b : any[]): Observable<any> {
    return this.http.post(conf.iinv,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveJenisDeposito(b : any[]): Observable<any> {
    return this.http.post(conf.ijenisdeposito,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveDeposito(b : any[]): Observable<any> {
    return this.http.post(conf.ideposito,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveHutang(b : any[]): Observable<any> {
    return this.http.post(conf.ihut,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveJenishutang(b : any[]): Observable<any> {
    return this.http.post(conf.ijenishut,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }
  saveNeraca(b : any[]): Observable<any> {
    return this.http.post(conf.ineraca,JSON.stringify(b),this.Auth()).map((res : Response) => res.json());
  }


 private Auth() {
    let headers = new Headers({ 'Authorization': this._auth.getToken() , 'Content-Type': 'application/json', 'Accept': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return options;
  }
}
