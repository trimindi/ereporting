import { Component, ViewEncapsulation, } from '@angular/core';
import {LocalDataSource, } from "ng2-smart-table";
import {InstanceService} from "./instance.service";
import {Response} from "@angular/http";
declare var alasql: any;

@Component({
  selector: 'instance',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./style.scss')],
  template: require('./instance.component.html'),
})
export class InstanceComponent {
  account : boolean = false;
  anggota : boolean = false;
  simpanan : boolean = false;
  jenissimpanan : boolean = false;
  hutang : boolean = false;
  jenishutang : boolean = false;
  deposito : boolean = false;
  jenisdeposito : boolean = false;
  inventaris : boolean = false;
  jenisinventaris : boolean = false;
  tabungan : boolean = false;
  jenistabungan : boolean = false;
  pinjaman : boolean = false;
  jenispinjaman : boolean = false;
  neraca : boolean = false;



  public alasql: any;
  balance : boolean = false;


  reset(){
    this.account  = false;
    this.anggota  = false;
    this.simpanan = false;
    this.jenissimpanan = false;
    this.hutang  = false;
    this.jenishutang  = false;
    this.deposito  = false;
    this.jenisdeposito  = false;
    this.inventaris= false;
    this.jenisinventaris  = false;
    this.tabungan  = false;
    this.jenistabungan  = false;
    this.pinjaman  = false;
    this.jenispinjaman  = false;
    this.neraca  = false;
  }

  constructor(private _instanceService: InstanceService) {
    this.alasql = alasql;

  }

  ngOnInit() {
    alasql('CREATE TABLE if not exists dd_komparasi(MASTER STRING,ACC STRING,KETERANGAN STRING,NERACA STRING,SALDO STRING)');
    alasql('CREATE TABLE if not exists ANGGOTA');
    alasql('CREATE TABLE if not exists ACCOUNT');
    alasql('CREATE TABLE if not exists SIMPANAN');
    alasql('CREATE TABLE if not exists HUTANG');
    alasql('CREATE TABLE if not exists JENISHUTANG');
    alasql('CREATE TABLE if not exists PIUTANG');
    alasql('CREATE TABLE if not exists JENISSIMP');
    alasql('CREATE TABLE if not exists JENISPIUTANG');
    alasql('CREATE TABLE if not exists JENISDEPOSITO');
    alasql('CREATE TABLE if not exists NERACA');
    alasql('CREATE TABLE if not exists INVENTARIS');
    alasql('CREATE TABLE if not exists JENISINVENTARIS');
    alasql('CREATE TABLE if not exists DEPOSITO');
    alasql('CREATE TABLE if not exists TABUNGAN');
    alasql('CREATE TABLE if not exists JENISTABUNGAN');

    alasql('DELETE FROM ANGGOTA');
    alasql('DELETE FROM dd_komparasi');
    alasql('DELETE FROM ACCOUNT');
    alasql('DELETE FROM SIMPANAN');
    alasql('DELETE FROM HUTANG');
    alasql('DELETE FROM JENISHUTANG');
    alasql('DELETE FROM PIUTANG');
    alasql('DELETE FROM JENISSIMP');
    alasql('DELETE FROM JENISPIUTANG');
    alasql('DELETE FROM JENISDEPOSITO');
    alasql('DELETE FROM NERACA');
    alasql('DELETE FROM INVENTARIS');
    alasql('DELETE FROM JENISINVENTARIS');
    alasql('DELETE FROM DEPOSITO');
    alasql('DELETE FROM TABUNGAN');
    alasql('DELETE FROM JENISTABUNGAN');
  }




  onTabungan($event) : void {
    if($event.target.value !== '') {
      alasql('DELETE FROM TABUNGAN');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO TABUNGAN SELECT * FROM ?',[res]);
          this.tabungan = true;
        }else{
        }
      });
    }
  }

  removeTabungan($event){
    alasql('DELETE FROM TABUNGAN');
    this.tabungan = false;
  }
  onJenisTabungan($event) : void {
    if($event.target.value !== '') {
      alasql('DELETE FROM JENISTABUNGAN');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO JENISTABUNGAN SELECT * FROM ?',[res]);
          this.jenistabungan = true;
        }else{
        }
      });
    }
  }

  removeJenisTabungan($event){
    alasql('DELETE FROM JENISTABUNGAN');
    this.jenistabungan = false;
  }


  onInventaris($event) : void {
    if($event.target.value !== '') {
      alasql('DELETE FROM INVENTARIS');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO INVENTARIS SELECT * FROM ?',[res]);
          this.inventaris = true;
        }else{
        }
      });
    }
  }

  removeInventaris($event) {
    alasql('DELETE FROM INVENTARIS');
    this.inventaris = false;
  }
  onJenisInventaris($event) : void {
    if($event.target.value !== '') {
      alasql('DELETE FROM JENISINVENTARIS');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO JENISINVENTARIS SELECT * FROM ?',[res]);
          this.jenisinventaris = true;
        }else{

        }
      });
    }
  }
  removeJenisInventaris($event){
    alasql('DELETE FROM JENISINVENTARIS');
    this.jenisinventaris = false;
  }

  onAnggota($event) : void {
    if($event.target.value !== '') {
      alasql('DELETE FROM ANGGOTA');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO ANGGOTA SELECT * FROM ?',[res]);
          this.anggota = true;
        }else{

        }
      });
    }
  }

  removeAnggota($event) {
    alasql('DELETE FROM ANGGOTA');
    this.anggota = false;
  }
  onAccount($event) : void {
    if($event.target.value !== '') {
      alasql('DELETE FROM ACCOUNT');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) =>  {
        if(res.length > 0){
          alasql('INSERT INTO ACCOUNT SELECT * FROM ?',[res]);
          this.account = true;
        }else{
        }
      });
    }
  }
  removeAccount($event){
    alasql('DELETE FROM ACCOUNT');
    this.account = false;
  }

  onSimpanan($event): void {
    if($event.target.value !== '') {
      alasql('DELETE FROM SIMPANAN');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO SIMPANAN SELECT * FROM ?',[res]);
          this.simpanan = true;
        }else{

        }
      });
    }
  }
  removeSimpanan($event){
    alasql('DELETE FROM SIMPANAN');
    this.simpanan = false;
  }

  onHutang($event): void {
    if($event.target.value !== '') {
      alasql('DELETE FROM HUTANG');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO HUTANG SELECT * FROM ?',[res]);
          this.hutang = true;
        }else{

        }
      });
    }
  }
  removeHutang($event){
    alasql('DELETE FROM HUTANG');
    this.hutang = false;
  }

  onPiutang($event): void {
    if($event.target.value !== '') {
      alasql('DELETE FROM PIUTANG');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO PIUTANG SELECT * FROM ?',[res]);
          this.pinjaman = true;
        }else{

        }
      });
    }
  }

  removePinjaman($event){
    alasql('DELETE FROM PIUTANG');
    this.pinjaman = false;
  }
  onJenisHutang($event): void {
    if($event.target.value !== '') {
      alasql('DELETE FROM JENISHUTANG');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO JENISHUTANG SELECT * FROM ?',[res]);
          this.jenishutang =true;
        }else{

        }
      });
    }
  }

  removeJenisHutang($event){
    alasql('DELETE FROM JENISHUTANG');
    this.jenishutang = false;
  }
  onJenisSimpanan($event): void {
    if($event.target.value !== '') {
      alasql('DELETE FROM JENISSIMP');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO JENISSIMP SELECT * FROM ?',[res]);
          this.jenissimpanan = true;
        }else{

        }
      });
    }
  }

  removeJenisSimpanan($event){
    alasql('DELETE FROM JENISSIMP');
    this.jenissimpanan = false;
  }
  onJenisPiutang($event): void {
    if($event.target.value !== '') {
      alasql('DELETE FROM JENISPIUTANG');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO JENISPIUTANG SELECT * FROM ?',[res]);
          this.jenispinjaman = true;
        }else{

        }
      });
    }
  }
  removeJenisPinjaman($event){
    alasql('DELETE FROM JENISPIUTANG');
    this.jenispinjaman = false;
  }
  onJenisDeposito($event): void {
    if($event.target.value !== ''){
      alasql('DELETE FROM JENISDEPOSITO');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO JENISDEPOSITO SELECT * FROM ?',[res]);
          this.jenisdeposito = true;
        }else{

        }
      });
    }
  }

  removeJenisDeposito($event){
    alasql('DELETE FROM JENISDEPOSITO');
    this.jenisdeposito = false;
  }

  onDeposito($event): void {
    if($event.target.value !== '') {
      alasql('DELETE FROM DEPOSITO');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})', [$event]).then((res)=> {
        if(res.length > 0){
          alasql('INSERT INTO DEPOSITO SELECT * FROM ?',[res]);
          this.deposito = true;
        }else{

        }
      });
    }
  }
  removeDeposito($event){
    alasql('DELETE FROM DEPOSITO');
    this.deposito = false;
  }
  onNeraca($event): void {
    if($event.target.value !== ''){
      alasql('DELETE FROM NERACA');
      alasql.promise('SELECT * FROM FILE(?,{headers:true})',[$event]).then((res) => {
        if(res.length > 0){
          alasql('INSERT INTO NERACA SELECT * FROM ?',[res]);
          this.neraca =true;
        }else{

        }
      });
    }
  }
  removeNeraca($event){
    alasql('DELETE FROM NERACA');
    this.neraca = false;
  }

  verivikasi(){
    alasql('DELETE FROM dd_komparasi');
    var simpanan = alasql('SELECT JENISSIMP.ACC as ACC,SUM(Number(SIMPANAN.SALDO)) AS SALDO FROM JENISSIMP LEFT JOIN SIMPANAN ON SIMPANAN.ACC = JENISSIMP.ACC GROUP BY JENISSIMP.ACC');
    alasql('INSERT INTO dd_komparasi VALUES("SIMPANAN","","","","")');
    if(simpanan.length > 0 && simpanan[0].ACC !== undefined){
      for (let key of simpanan) {
        var acc = key.ACC;
        var saldo = key.SALDO;
        var insert = alasql('SELECT * FROM NERACA WHERE ACC = '+acc);
        console.log(insert);
        var ner = insert[0].DEBET + insert[0].KREDIT;
        console.log(ner);
        alasql('INSERT INTO dd_komparasi VALUES ?',[{MASTER: "",ACC:acc, KETERANGAN:insert[0].KETERANGAN , NERACA : ner,SALDO : saldo}]);
      }
    }

    var hutang = alasql('SELECT JENISHUTANG.ACC as ACC,SUM(Number(HUTANG.OUTSTANDING)) AS SALDO FROM JENISHUTANG LEFT JOIN HUTANG ON HUTANG.ACC = JENISHUTANG.ACC GROUP BY JENISHUTANG.ACC');
    alasql('INSERT INTO dd_komparasi VALUES("HUTANG","","","","")');
    if(hutang.length > 0 && hutang[0].ACC !== undefined){
      for (let key of hutang) {
        var acc = key.ACC;
        var saldo = key.SALDO;
        var insert = alasql('SELECT * FROM NERACA WHERE ACC = '+acc);
        console.log(insert);
        var ner = insert[0].DEBET + insert[0].KREDIT;
        console.log(ner);
        alasql('INSERT INTO dd_komparasi VALUES ?',[{MASTER: "",ACC:acc, KETERANGAN:insert[0].KETERANGAN , NERACA : ner,SALDO : saldo}]);
      }
    }
    var piutang = alasql('SELECT JENISPIUTANG.ACC,SUM(Number(PIUTANG.OUTSTANDING)) AS SALDO FROM JENISPIUTANG LEFT JOIN PIUTANG ON PIUTANG.ACC = JENISPIUTANG.ACC GROUP BY JENISPIUTANG.ACC');
    alasql('INSERT INTO dd_komparasi VALUES("PIUTANG","","","","")');
    if(piutang.length > 0 && piutang[0].ACC !== undefined){
      for (let key of piutang) {
        var acc = key.ACC;
        var saldo = key.SALDO;
        var insert = alasql('SELECT * FROM NERACA WHERE ACC = '+acc);
        console.log(insert);
        var ner = insert[0].DEBET + insert[0].KREDIT;
        console.log(ner);
        alasql('INSERT INTO dd_komparasi VALUES ?',[{MASTER: "",ACC:acc, KETERANGAN:insert[0].KETERANGAN , NERACA : ner,SALDO : saldo}]);
      }
    }
    var deposito = alasql('SELECT JENISDEPOSITO.ACC AS ACC,SUM(Number(DEPOSITO.OUTSTANDING)) AS SALDO FROM JENISDEPOSITO LEFT JOIN DEPOSITO ON DEPOSITO.ACC = JENISDEPOSITO.ACC GROUP BY JENISDEPOSITO.ACC');
    // console.log(deposito)
    alasql('INSERT INTO dd_komparasi VALUES("DEPOSITO","","","","")');
    if(deposito.length > 0 && deposito[0].ACC !== undefined){
      for (let key of deposito) {
        var acc = key.ACC;
        var saldo = key.SALDO;
        var insert = alasql('SELECT * FROM NERACA WHERE ACC = '+acc);
        console.log(insert);
        var ner = insert[0].DEBET + insert[0].KREDIT;
        console.log(ner);
        alasql('INSERT INTO dd_komparasi VALUES ?',[{MASTER: "",ACC:acc, KETERANGAN:insert[0].KETERANGAN , NERACA : ner,SALDO : saldo}]);
      }
    }
    var tabungan = alasql('SELECT JENISTABUNGAN.ACC,SUM(Number(TABUNGAN.SALDO)) AS SALDO FROM JENISTABUNGAN LEFT JOIN TABUNGAN ON TABUNGAN.ACC = JENISTABUNGAN.ACC GROUP BY JENISTABUNGAN.ACC');
    // console.log(tabungan);
    alasql('INSERT INTO dd_komparasi VALUES("TABUNGAN","","","","")');
    if(tabungan.length > 0 && tabungan[0].ACC !== undefined){
      for (let key of tabungan) {
        var acc = key.ACC;
        var saldo = key.SALDO;
        var insert = alasql('SELECT * FROM NERACA WHERE ACC = '+acc);
        console.log(insert);
        var ner = insert[0].DEBET + insert[0].KREDIT;
        console.log(ner);
        alasql('INSERT INTO dd_komparasi VALUES ?',[{MASTER: "",ACC:acc, KETERANGAN:insert[0].KETERANGAN , NERACA : ner,SALDO : saldo}]);
      }
    }

    var inventaris = alasql('SELECT JENISINVENTARIS.ACC,SUM(Number(INVENTARIS.NBUKU)) AS SALDO FROM JENISINVENTARIS LEFT JOIN INVENTARIS ON INVENTARIS.ACC = JENISINVENTARIS.ACC GROUP BY JENISINVENTERIS.ACC');
    alasql('INSERT INTO dd_komparasi VALUES("INVENTARIS","","","","")');
    if(inventaris.length > 0 && inventaris[0].ACC !== undefined){
      for (let key of inventaris) {
        var acc = key.ACC;
        var saldo = key.SALDO;
        var insert = alasql('SELECT * FROM NERACA WHERE ACC = '+acc);
        console.log(insert);
        var ner = insert[0].DEBET + insert[0].KREDIT;
        console.log(ner);
        alasql('INSERT INTO dd_komparasi VALUES ?',[{MASTER: "",ACC:acc, KETERANGAN:insert[0].KETERANGAN , NERACA : ner,SALDO : saldo}]);
      }
    }
    let fetch = alasql('SELECT MASTER,ACC,KETERANGAN,SALDO,NERACA,(SALDO - NERACA) AS SELISIH FROM dd_komparasi');
    let valid = alasql('SELECT sum(SALDO - NERACA) AS SELISIH  FROM dd_komparasi');
    if(valid[0].SELISIH == 0){
      alert("data anda valid");
      this.source.load(fetch);
      this.balance = true;

    }else{
      this.source.load(fetch);
      this.balance = false;
      alert("data anda belum valid");
    }
    console.log(fetch)
  }

  handleError(err:any){
    console.error(err);
  }

  handleResponse(res : Response,variable : any){
    if(res.status == 401){
    }else{
      variable = false;
    }
  }


  sendData(){

    let jenissimpanan = alasql('SELECT * FROM JENISSIMP');
    if(jenissimpanan.length > 0) this._instanceService.saveJenisSimpanan(jenissimpanan)
      .subscribe(res => this.handleResponse(res,this.jenissimpanan),err => this.handleError(err));

    let simpanan = alasql('SELECT * FROM SIMPANAN');
    if(simpanan.length > 0) this._instanceService.saveSimpanan(simpanan)
      .subscribe(res => this.handleResponse(res,this.simpanan),err => this.handleError(err));

    let jenispiut = alasql('SELECT * FROM JENISPIUTANG');
    if(jenispiut.length > 0) this._instanceService.saveJenisPiutang(jenispiut)
      .subscribe(res => this.handleResponse(res,this.jenispinjaman),err => this.handleError(err));

    let piut = alasql('SELECT * FROM PIUTANG');
    if(piut.length > 0) this._instanceService.savePiutang(piut)
      .subscribe(res => this.handleResponse(res,this.pinjaman),err => this.handleError(err));

    let anggota = alasql('SELECT * FROM ANGGOTA');
    if(anggota.length > 0) this._instanceService.saveAnggota(anggota)
      .subscribe(res => this.handleResponse(res,this.anggota),err => this.handleError(err));

    let account = alasql('SELECT * FROM ACCOUNT');
    if(account.length > 0) this._instanceService.saveAccount(account)
      .subscribe(res => this.handleResponse(res,this.account),err => this.handleError(err));

    let deposito = alasql('SELECT * FROM DEPOSITO');
    if(deposito.length > 0) this._instanceService.saveDeposito(deposito)
      .subscribe(res => this.handleResponse(res,this.deposito),err => this.handleError(err));

    let jenisdeposito = alasql('SELECT * FROM JENISDEPOSITO');
    if(jenisdeposito.length > 0) this._instanceService.saveDeposito(jenisdeposito)
      .subscribe(res => this.handleResponse(res,this.jenisdeposito),err => this.handleError(err));

    let tabungan = alasql('SELECT * FROM TABUNGAN');
    if(tabungan.length > 0) this._instanceService.saveTabungan(tabungan)
      .subscribe(res => this.handleResponse(res,this.tabungan),err => this.handleError(err));

    let jenistabungan = alasql('SELECT * FROM JENISTABUNGAN');
    if(jenistabungan.length > 0) this._instanceService.saveJenisTabungan(jenistabungan)
      .subscribe(res => this.handleResponse(res,this.jenistabungan),err => this.handleError(err));

    let hutang = alasql('SELECT * FROM HUTANG');
    if(hutang.length > 0) this._instanceService.saveHutang(hutang)
      .subscribe(res => this.handleResponse(res,this.hutang),err => this.handleError(err));

    let jenishutang = alasql('SELECT * FROM JENISHUTANG');
    if(jenishutang.length > 0) this._instanceService.saveJenishutang(jenishutang)
      .subscribe(res => this.handleResponse(res,this.jenishutang),err => this.handleError(err));

    let inv = alasql('SELECT * FROM INVENTARIS');
    if(inv.length > 0) this._instanceService.saveInventaris(inv)
      .subscribe(res => this.handleResponse(res,this.inventaris),err => this.handleError(err));

    let jenisinv = alasql('SELECT * FROM JENISINVENTARIS');
    if(jenisinv.length > 0) this._instanceService.saveJenisInventaris(jenisinv)
      .subscribe(res => this.handleResponse(res,this.jenisinventaris),err => this.handleError(err));
    let neraca = alasql('SELECT * FROM NERACA');
    if(neraca.length > 0) this._instanceService.saveNeraca(neraca)
      .subscribe(res => this.handleResponse(res,this.neraca),err => this.handleError(err));
    alasql('SELECT * INTO XLSX("neraca.xlsx",{headers:true}) FROM ?',[neraca]);
    this.reset();
  }




  query: string = '';


  settings = {
    actions : {
      hideSubHeader: true,
      add: false,
      edit: false,
      delete: false

    },
    columns: {
      MASTER: {
        title: 'MASTER',
        type: 'string'
      },
      ACC: {
        title: 'ACC',
        type: 'string'
      },
      KETERANGAN: {
        title: 'KETERANGAN',
        type: 'string'
      },
      NERACA: {
        title: 'NERACA',
        type: 'string'
      },
      SALDO: {
        title: 'SALDO',
        type: 'string'
      },
      SELISIH: {
        title: 'SELISIH',
        type: 'string'
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();
}
