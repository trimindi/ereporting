export class Deposito {
  NODEP: string;
  ACC: string;
  CIB: string;
  BILYET: string;
  TGLBUKA: string;
  TGLAKTIF: string;
  TGLCAIR: string;
  REKST: string;
  NOSIMP: string;
  JW: string;
  PB: string;
  RO: string;
  BGA: string;
  BUNGA: string;
  PJK: string;
  PAJAK: string;
  PEN: string;
  PENALTY: string;
  NOMINAL: string;
  ST: string;
  TGL_APV: string;
  UID: string;
  KU: string;
  CIF: string;
}
