export class Simpanan {
  NOSIMP: string;
  ACC: string;
  CIB: string;
  DEBET: string;
  KREDIT: string;
  BLOKIR: string;
  SALDO: string;
  TGLBUKA: string;
  TGLAKTIF: string;
  TGLBEBAN: string;
  ST: string;
  KU: string;
  CIF: string;
}
