import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { routing }       from './admin.routing';
import { NgaModule } from '../theme/nga.module';

import { AdminComponent } from './admin.component';
import { AuthGuard } from '../_guard/auth.guard';

@NgModule({
  imports: [CommonModule, NgaModule, routing],
  declarations: [AdminComponent],
  providers:[AuthGuard]
})
export class AdminModule {
}
