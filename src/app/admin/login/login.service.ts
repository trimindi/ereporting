import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import conf = require('./../../globals.ts');
import {CookieService} from "angular2-cookie/services/cookies.service";



@Injectable()
export class AuthService {

    constructor(
        private _cookieService: CookieService,
        private _http: Http) {
    }

    login(username : any,password : any):Observable<any>{
        var data = {
          username : username,
          password : password
        }
        return this._http.post(conf.auth,JSON.stringify(data),this.Auth())
          .map((res : Response) => res.json());

    }

    getToken():string{
      return JSON.parse(this._cookieService.get('user')).token;
    }

    private Auth() {
      let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return options;
    }
}
