import { Component, ViewEncapsulation } from '@angular/core';

import {Router, Routes} from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import {AuthService} from "./login.service";
import {CookieService} from "angular2-cookie/services/cookies.service";
import {BaMenuService} from "../../theme/services/baMenu/baMenu.service";
import {AppMenu} from "../../app.menu";


@Component({
  selector: 'login',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./login.scss')],
  template: require('./login.html'),
})
export class Login {
  massage : string;
  public form: FormGroup;
  public email: AbstractControl;
  public password: AbstractControl;
  public submitted: boolean = false;

  constructor(private _cookieService: CookieService,
              private fb: FormBuilder,
              private _route: Router,
              private _appMenu : AppMenu,
              private _menuService: BaMenuService,
              private _auth : AuthService) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });

    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
  }

  public onSubmit(values: Object): void {
    this.submitted = true;

    console.log(this.email,this.password)
    if (this.form.valid) {
      this._auth.login(this.email.value,this.password.value)
        .subscribe(login => {
          if(login.status == 401){
            this.massage = "Username / Password Salah!";
          }
          if(login.success){
              this._cookieService.put('user',JSON.stringify(login));
              this._menuService.updateMenuByRoutes(<Routes>this._appMenu.getMenu());
              this._route.navigate(['/dashboard']);
          }
        },err => this.massage = "Terjadi kesalahan saat menghubungu server.");
    }
  }
}
