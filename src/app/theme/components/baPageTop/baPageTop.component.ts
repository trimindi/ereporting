import {Component, Output, EventEmitter} from '@angular/core';

import {GlobalState} from '../../../global.state';
import 'style-loader!./baPageTop.scss';
import {CookieService} from "angular2-cookie/services/cookies.service";

@Component({
  selector: 'ba-page-top',
  templateUrl: './baPageTop.html',
})
export class BaPageTop {

  @Output() logoutAction = new EventEmitter();

  public isScrolled:boolean = false;
  public isMenuCollapsed:boolean = false;

  constructor(private _state:GlobalState,private _cookieService : CookieService) {
    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });
  }

  public toggleMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
    return false;
  }

  public scrolledChanged(isScrolled) {
    this.isScrolled = isScrolled;
  }
  logout(){
    this.logoutAction.emit('logout');
  }
}
